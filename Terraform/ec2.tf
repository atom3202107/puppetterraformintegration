provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "terraform-server" {
    ami = "ami-0440d3b780d96b29d"
    instance_type = "t2.micro"
    key_name = "ec2terraform"
    tags = {
      Name = "terraform"
    }

    provisioner "local-exec" {
      command = "cmd.exe /c G:\\worldline\\script.sh"
    }

}